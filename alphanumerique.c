/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3   							      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Lecrivain Maxime                                             *
*                                                                             *
*  Nom-prénom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : alphanumerique.c                                          *
*                                                                             *
******************************************************************************/

#include <wchar.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include "alphanumerique.h"

wchar_t accent[17]= {L'à',L'â',L'ä',L'é',L'è',L'ê',L'ë',L'î',L'ï',L'ô',L'ö',L'ù',L'û',L'ü',L'ç'};
wchar_t replace[17]= {L'a',L'a',L'a',L'e',L'e',L'e',L'e',L'i',L'i',L'o',L'o',L'u',L'u', L'u', L'c'};
wchar_t carspe[19]= {L'/',L'<',L'>',L'#',L'(',L'{',L'[',L']',L'}',L')',L'*',L'-',L'+',L'.',L'@',L'|',L'&',L'~',L'"'};

int accentcar(wchar_t *mot)
{

        for(int i=0; i<wcslen(mot); i++)
        {
                for(int c=0; c<wcslen(carspe); c++)
                {
                        if(mot[i]==carspe[c])
                        {
                                return -1;
                        }
                }
        }
        caractcar(mot);
	return 1;
}

void caractcar(wchar_t *mot)
{

                for(int i=0; i<wcslen(mot); i++) {
                        for(int b=0; b<wcslen(accent); b++){
                                if(mot[i] == accent[b])
                                {
                                        mot[i]=replace[b];
                                }
                        }
                }
                wprintf(L"mot sans accent: %ls\n",mot);
}

