/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3   							      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Lecrivain Maxime                                             *
*                                                                             *
*  Nom-prénom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : vigenere.c                                                *
*                                                                             *
******************************************************************************/

#include <wchar.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include "vigenere.h"

int rang(char lettre)
{
        char alphabet[26] = "abcdefghijklmnopqrstuvwxyz";
        int i;
        for (i = 0; i < 26; i++)
            if (lettre == alphabet[i])
                return i;
}

int rangMAJ(char lettre)
{
        char alphabet[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int i;
        for (i = 0; i < 26; i++)
            if (lettre == alphabet[i])
                return i;
}


void vigenere(wchar_t *phrase, wchar_t *cle, wchar_t *resultat, int crypte)
{
        int i_cl=0, y, z, x;
        char alphabet[27] = "abcdefghijklmnopqrstuvwxyz";
        char alphabet2[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int l_phrase = wcslen(phrase);
        int l_cle = wcslen(cle);
        for (int i_ph = 0; i_ph < l_phrase-1; i_ph++)
        {
      x = rang(cle[i_cl]);
	if(phrase[i_ph]!=' ')
	{
      		i_cl = (i_cl+1) % l_cle;
	}
      	if (crypte)
        {
        if((L'a'<=phrase[i_ph]) && (L'z'>=phrase[i_ph]))
        {
        z = rang(phrase[i_ph]);
        }
        else if(phrase[i_ph] == ' '){
        z = 0;
        }
        else
        {
        z = rangMAJ(phrase[i_ph]);
        }
        y =(z+x)%26;
      }
      else
      {
        if((L'a'<=phrase[i_ph]) && (L'z'>=phrase[i_ph]))
        {
        z = rang(phrase[i_ph]);
        }
        else if(phrase[i_ph] == ' ')
        {
        z = 0;
        }
        else
        {
        z = rangMAJ(phrase[i_ph]);
        }
       y = (z - x) ;
       if (y < 0)
        y += 26;
      }

        if((L'a'<=phrase[i_ph]) && (L'z'>=phrase[i_ph]))
        {
                resultat[i_ph]=alphabet[y];
        }
        else if(z == 0)
        {
                resultat[i_ph]=' ';
        }
        else
        {
                resultat[i_ph]=alphabet2[y];
        }

    }
}



