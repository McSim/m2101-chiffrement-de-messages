M2101 Sujet 3 Chiffrement de messages
LECRIVAIN Maxime S2F.

L'objectif de l'application est de pouvoir traiter un texte écrit par l'utilisateur en trois étapes : 
	- Vérifier les caractères spéciaux 
	- Enlever les accents
	- Laisser le choix à l'utilisateur de 2 algorithmes de chiffrement pour chiffrer ou déchiffrer son texte avec une clé de son choix.

Le fichier alphanumérique.c possède 2 fonctions : 
	
	int accentcar(wchar_t *mot)
	- Possède en entrée un texte en wchar_t* qui est le texte précédemment saisi par l'utilisateur lors du lancement du programme.
   	- Il est attendu qu'il retourne -1 si un caractère spécial est trouvé soit /<>#({[]})*-+.@|&~"  
    	- Si aucun caractère spécial n'est trouvé alors il appelle la procédure caractcar et retourne 1.
    
	void caractcar(wchar_t *mot)
    	- Possède en entrée un texte en wchar_t* qui est le texte précédemment saisi par l'utilisateur lors du lancement du programme.
    	- Il est attendu qu'il modifie le mot sans accent.

Le fichier cesar.c possède 2 fonctions :
	
	void cesar_crypt(int decalage, wchar_t *texte)
   	- Possède en entrée un texte en wchar_t* qui est le texte précédemment saisi par l'utilisateur lors du lancement du programme et sans accent. Il possède aussi la clé pour le décalage voulu.
   	- Il est attendu qu'il modifie le texte grâce au décalage pour le crypter.
   
	void cesar_decrypt(int decalage, wchar_t *texte)
   	- Possède en entrée un texte en wchar_t* qui est le texte précédemment saisi par l'utilisateur lors du lancement du programme et sans accent. Il possède aussi la clé pour le décalage voulu.
  	- Il est attendu qu'il modifie le texte grâce au décalage pour le décrypter.

Le fichier vigenere.c possède 3 fonctions :
	
	int rang(char lettre);
   	- Possède en entrée une lettre en minuscule et retourne sa position dans l'alphabet.
   
	int rangMAJ(char lettre);
   	- Possède en entrée une lettre en majuscule et retourne sa position dans l'alphabet.
   
	void vigenere(wchar_t *phrase, wchar_t *cle, wchar_t *resultat, int crypte)
    	- Possède en entrée la phrase à crypter ou à décrypter, la clé et une valeur "crypte" pour indiquer si il faut crypter ou décrypter le message.
    	- En sortie le résultat du cryptage ou décryptage.
