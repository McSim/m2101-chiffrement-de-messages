/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3   							      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Lecrivain Maxime                                             *
*                                                                             *
*  Nom-prénom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : cesar.c                                                   *
*                                                                             *
******************************************************************************/

#include <wchar.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include "cesar.h"

void cesar_crypt (int decalage, wchar_t* texte)
{
        for(int i=0 ; i<wcslen(texte)-1 ; i++){
        if ('a' <= texte[i] && texte[i] <= 'z'){
            texte[i] = ((texte[i] - 'a' + decalage)%26);
            if(texte[i]<0){
                texte[i]+=26;
            }
            texte[i]= texte[i] + 'a';
        }else if (texte[i] == ' '){
                texte[i] = ' ';
            }else{
                    texte[i] = ((texte[i] - 'A' + decalage)%26);
                    if(texte[i]<0){
                        texte[i]+=26;
                    }
                    texte[i]= texte[i]+'A';
                }
        }


}

void cesar_decrypt (int decalage, wchar_t* texte)
{
    for(int i=0 ; i<wcslen(texte)-1 ; i++){
        if ('a' <= texte[i] && texte[i] <= 'z'){
            texte[i] = ((texte[i] - 'a' - decalage)%26);
			if(texte[i]<0){
				texte[i]+=26;
			}
			texte[i]= texte[i] + 'a';
		}else if (texte[i] == ' '){
            texte[i] = ' ';
            }else{
				texte[i] = ((texte[i] - 'A' - decalage)%26);
				if(texte[i]<0){
					texte[i]+=26;
				}
				texte[i]= texte[i]+'A';
			}
	}
}
