/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3   							      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Lecrivain Maxime                                             *
*                                                                             *
*  Nom-prénom2 :                                                              *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

#include <wchar.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include "cesar.h"
#include "vigenere.h"
#include "alphanumerique.h"

void main()
{
	setlocale(LC_ALL, "");
	FILE* fichier = NULL;

	fichier = fopen("resultat.txt", "w");

	if(fichier != NULL)
	{
        wchar_t mot[300];
	int error = 0;
        wprintf(L"Ecrire le mot :");
	fgetws(mot, 300, stdin);
	fputws(L"Phrase avec accent : ", fichier);
	fputws(mot, fichier);
	if(accentcar(mot) == 1)
	{
	fputws(L"\nPhrase sans accent : ", fichier);
	fputws(mot, fichier);
	wprintf(L"%ls", mot);
	int choix;
	wprintf(L"Choisissez la méthode de chiffrement a utiliser: César[1], Vigenère[2]\n");
	wscanf(L"%d", &choix);

	if(choix==1){

		int k;
        	wprintf(L"Saisir clé (numérique)\n");
        	wscanf(L"%d",&k);
              	fputws(L"\nLa clé saisi est : ", fichier);
       	        fwprintf(fichier,L"%d",  k);

		wprintf(L"Vous voulez chiffer[1] ou déchiffrer[2] le message?\n");
		wscanf(L"%d",&choix);
			if(choix==1)
			{
				cesar_crypt(k, mot);
				wprintf(L"mot crypter: %ls\n", mot);
				fputws(L"\nPhrase cryptée avec César :", fichier);
				fputws(mot, fichier);
			}
			else if (choix==2){
				cesar_decrypt(k, mot);
				wprintf(L"mot decrypter: %ls\n", mot);
				fputws(L"\nPhrase decryptée avec César: ", fichier);
				fputws(mot, fichier);
			}
	}
	else if(choix==2)
	{
		wchar_t charCle[200];
		wprintf(L"Saisir clé (sans espace)\n");
		wscanf(L"%ls", &charCle);
		fputws(L"\nLa clé saisi est : ", fichier);
                fputws(charCle, fichier);

		wprintf(L"Vous voulez chiffer[1] ou déchiffrer[2] le message?\n");
                wscanf(L"%d",&choix);
                        if(choix==1)
                        {
                                vigenere(mot, charCle, mot, 1);
                                wprintf(L"mot crypter: %ls\n", mot);
                                fputws(L"\nPhrase cryptée avec Vigenère:", fichier);
                                fputws(mot, fichier);
                        }
                        else if (choix==2){
                                vigenere(mot, charCle, mot, 0);
                                wprintf(L"mot decrypter: %ls\n", mot);
                                fputws(L"\nPhrase decryptée avec Vigenère: ", fichier);
                                fputws(mot, fichier);
                        }
	}
	fclose(fichier);
	}
	else
	{
		wprintf(L"Error");
	}
}
}
